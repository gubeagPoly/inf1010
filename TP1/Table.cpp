#include "Table.h"

Table::Table() : capacite_(MAXCAP), id_(-1), nbPlaces_(1), nbPlats_(0), occupee_(false)
{
	commande_ = new Plat *[MAXPLAT];
	for (int i = 0; i < MAXPLAT; i++)
	{
		commande_[i] = new Plat();
	}
}

Table::Table(int id, int nbPlaces) : Table()
{
	id_ = id;
	nbPlaces_ = nbPlaces;
}

int Table::getId() const
{
	return id_;
}

int Table::getNbPlaces() const
{
	return nbPlaces_;
}

bool Table::estOccupee() const
{
	return occupee_;
}

void Table::libererTable()
{
	for (int i = 0; i < nbPlaces_; i++)
	{
		delete commande_[i];
		commande_[i] = nullptr;
	}
	nbPlaces_ = 0;
	nbPlats_ = 0;
	occupee_ = false;
}

void Table::placerClient()
{
	occupee_ = true;
}

void Table::setId(int id)
{
	id_ = id;
}

void Table::commander(Plat* plat)
{
	commande_[nbPlats_] = plat;
	nbPlats_++;
}

double Table::getChiffreAffaire() const
{
	double gains = 0;
	for (int i = 0; i < nbPlats_; i++)
	{
		gains += commande_[i]->getPrix() - commande_[i]->getCout();
	}
	return gains;
}

void Table::afficher() const
{
	std::cout << "La table numero " << id_ << " est occupee. Voici la commande passee par les clients : " << endl;
	for (int i = 0; i < nbPlats_; i++) 
	{
		commande_[i]->afficher();
	}
}