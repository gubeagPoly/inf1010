#include "Menu.h"

Menu::Menu()
	: capacite_(MAXPLAT), listePlats_(new Plat*[MAXPLAT]), nbPlats_(0), type_(Matin) {}

Menu::Menu(string fichier, TypeMenu type)
	: Menu() {
	type_ = type;
	if (!lireMenu(fichier))
		cout << "ERREUR : " << fichier << " ne contient pas de menu.";
}

unsigned int Menu::getCapacite() const {
	return capacite_;
}

Plat** Menu::getListePlats() const {
	return copierListePlat(listePlats_, capacite_);
}

unsigned int Menu::getNbPlats() const {
	return nbPlats_;
}

TypeMenu Menu::getTypeMenu() const {
	return type_;
}

void Menu::setCapacite(unsigned int capacite, bool &echec) {
	setListePlats(listePlats_, capacite, echec);
}

void Menu::setListePlats(Plat** listePlats, unsigned int capacite, bool &echec) {
	if (!estBonneCapacite(capacite))
		echec = true;
	else {
		echec = false;
		if (capacite != capacite_) {
			listePlats_ = copierListePlat(listePlats_, capacite);
			capacite_ = capacite;
		}
	}
}

void Menu::setType(TypeMenu type) {
	type_ = type;
}

// ----- m�thodes publiques ----- //

void Menu::afficher() const {
	cout << typeMenu[type_] << endl;
	for (int i = 0; i < nbPlats_; i++)
		listePlats_[i]->afficher();
	cout << endl;
}

Plat* Menu::trouverPlat(string& nom) const {
	Plat* ptrPlat = nullptr;
	for (int i = 0; i < nbPlats_; i++) {
		if (listePlats_[i]->getNom == nom)
			ptrPlat = listePlats_[i];
	}
	return ptrPlat;
}

void Menu::ajouterPlat(Plat& plat) {
	if (nbPlats_ < MAXPLAT) {
		listePlats_[nbPlats_] = &plat;
		nbPlats_++;
	}
}

void Menu::ajouterPlat(string& nom, double montant, double cout) {
	ajouterPlat(Plat(nom, montant, cout));
}

bool Menu::lireMenu(string& fichier) {
	ifstream fichierMenu(fichier);

	//Descente vers le menu de bon type dans le .txt
	string ligne;
	while (ligne != typeMenu[type_]) {
		getline(fichierMenu, ligne);
		if (fichierMenu.eof()) {
			fichierMenu.close();
			return false;
		}
	}
	fichierMenu.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	//Ajout des plats au menu
	while (fichierMenu.peek() == int("-")) {
		string nom; double montant, cout;
		fichierMenu >> nom >> montant >> cout;
		ajouterPlat(nom, montant, cout);
		fichierMenu.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
	}

	fichierMenu.close();
	return true;
}

bool Menu::estBonneCapacite(unsigned int capacite) const {
	return capacite >= 1 && capacite <= MAXPLAT && capacite >= nbPlats_;
}

// ----- m�thodes priv�es ----- //

Plat** Menu::copierListePlat(Plat** listePlats, unsigned int capacite) const {
	Plat** copie = new Plat*[capacite];
	for (int i = 0; i < nbPlats_; i++)
		copie[i] = new Plat(*listePlats[i]);
	return copie;
}