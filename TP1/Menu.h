/*
* Titre : Menu.h - Travail Pratique #1
* Date : 09 Janvier 2019
* Auteur : David Dratwa
*/

#ifndef MENU_H
#define MENU_H 

#include "Plat.h"
#include <fstream>

enum TypeMenu { Matin, Midi, Soir };
const string typeMenu[] = { "-MATIN", "-MIDI", "-SOIR" };
const int MAXPLAT = 5;
class Menu {
public:
	// constructeurs
	Menu();
	Menu(string fichier, TypeMenu type);

	//getters
	unsigned int getCapacite() const;
	Plat** getListePlats() const;
	unsigned int getNbPlats() const;
	TypeMenu getTypeMenu() const;

	//setters
	void setCapacite(unsigned int capacite, bool &echec);
	void setListePlats(Plat** listePlats, unsigned int capacite, bool &echec);
	// ? void setNbPlats(unsigned int nbPlats);
	void setType(TypeMenu type);

	//affichage 
	void afficher() const;

	//methodes en plus
	Plat* trouverPlat(string& nom) const;
	void ajouterPlat(Plat & plat);
	void ajouterPlat(string& nom, double montant, double cout);
	bool lireMenu(string& fichier);
	bool estBonneCapacite(unsigned int capacite) const;

private:
	//attributs
	unsigned int capacite_;
	Plat** listePlats_;
	unsigned int nbPlats_;
	TypeMenu type_;

	//methodes en plus
	Plat** copierListePlat(Plat** listePlats, unsigned int capacite) const;
};

#endif // !MENU_H
