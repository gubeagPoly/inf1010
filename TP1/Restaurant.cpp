#include "Restaurant.h"

Restaurant::Restaurant() : nom_(nullptr), chiffreAffaire_(0.0)
						 , momentJournee_(Matin), menuMatin_(nullptr)
						 , menuMidi_(nullptr), menuSoir_(nullptr)
						 , capaciteTables_(MAXCAP)
{
	nbTables_ = 1;
	tables_ = new Table*[nbTables_];
}
Restaurant::Restaurant(string& fichier, string& nom, TypeMenu moment) : Restaurant()
{
	nom_ = & nom;
	momentJournee_ = moment;
	lireTable(fichier);
}

void Restaurant::setMoment(TypeMenu moment)
{
	momentJournee_ = moment;
}

string Restaurant::getNom()
{
	return *nom_;
}

TypeMenu Restaurant::getMoment()
{
	return momentJournee_;
}

void Restaurant::lireTable(string& fichier) 
{
	ifstream fResto(fichier);

	string ligne;
	while (ligne.find("TABLES") == std::string::npos)
	{
		getline(fResto, ligne);
	}
	int idBuffer, nbPlaceBuffer = 0;

	while (fResto.peek() != EOF)
	{
		fResto >> idBuffer >> nbPlaceBuffer;
		ajouterTable(idBuffer, nbPlaceBuffer);
		//SkipLine
		getline(fResto, ligne);
	}
}

void Restaurant::ajouterTable(int id, int nbPlace)
{
	
}

void Restaurant::grandirNbTable() 
{
	 for(int i = 0; i < nbTables_; i++)
	 {
		 delete tables_[i];
		 tables_[i] = nullptr;
	 }
	 delete[] tables_;

	 nbTables_++;
	 tables_ = new Table *[nbTables_];
}